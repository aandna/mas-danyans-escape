// Set the date we're counting down to
var countDownDate = new Date("Jul 5, 2018 23:06:25").getTime();
var pistaNum = 1;
var pistaNum2 = 1;

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Output the result in an element with id="demo"
    document.getElementById("countdown").innerHTML = minutes + "m " + seconds + "s ";

    // If the count down is over, write some text
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "EXPIRED";
    }
}, 1000);

function nextPista() {
    pistaNum++;
    switch (pistaNum) {
        case 1:
            $("#pista-text").html("Sota la pica trobaràs allò que et marcarà el compàs, però abans una endivinalla et trobaràs. És fàcil de solucionar, és només l'any en què tons pares es van casar.")
            $("#pista-input-container").html('<input id="input-1" type="text" class="form-control">');
            break;
        case 2:
            if($("#input-1").val() == "1960") {
                $("#pista-text").html("Correcte! Ja pots mirar què hi ha sota la pica.")
                $("#pista-input-container").html("");
            } else {
                window.location.replace("blank.html");
            }
            break;
        case 3:
            $("#pista-text").html("Si és cert que el que s'amagava has trobat, selecciona el que és.")
            $("#pista-input-container").html('<select class="custom-select" id="select-1">    <option selected="">Escull...</option>     <option value="1">Marcapassos</option>    <option value="2">Vaca</option>    <option value="3">iPod</option>    <option value="4">CD</option>  </select>');
            break;
        case 4:
            if($("#select-1").val() == "4") {
                $("#pista-text").html("Doncs sí, sembla que sí que havies trobat el CD. Però, et diu alguna cosa? Hauràs de tirar de memòria fotogràfica, perquè és la pista fins a la següent prova.")
                $("#pista-input-container").html("");
            } else {
                window.location.replace("blank.html");
            }
            break;
        case 5:
            $("#pista-text").html("Ai les dates, les dates... Cinc nombres, quin en serà l'ordre correcte? Quan el tinguis introdueix els nombres tots seguits (ex. 4815162342).")
            $("#pista-input-container").html('<input id="input-2" type="text" class="form-control">');
            break;
        case 6:
            if($("#input-2").val() == "17135518") {
                $("#pista-text").html("Sí!! Els dies en què van néixer els teus nebots. Ai quin descans... Encara que ja t'avanço que no serà l'última prova de dates que et trobaràs. Va, per relaxar-te ves amb ells a jugar a l'esport que tots practiqueu.")
                $("#pista-input-container").html("");
            } else {
                window.location.replace("blank.html");
            }
            break;
        case 7:
            $("#pista-text").html("No me'n fio... Realment ets on hauries de ser? A veure, de quin color són les porteries?")
            $("#pista-input-container").html('<select class="custom-select" id="select-2">    <option selected="">Escull...</option>     <option value="1">Blanc</option>    <option value="2">Verd</option>    <option value="3">Vermell</option>    <option value="4">Negre</option>  </select>');
            break;
        case 8:
            window.location.replace("basket.html");
            break;
        default:

    }
}

function nextPista2() {
    pistaNum2++;
    switch (pistaNum2) {
        case 1:
            $("#pista-text").html("Quin és l'objecte que has trobat a la pista de bàsket?")
            $("#pista-input-container").html('<select class="custom-select" id="select-3">    <option selected="">Escull...</option>     <option value="1">Revista</option>    <option value="2">Sobre de carta</option>    <option value="3">Llaminadures</option>    <option value="4">Joc de Taula</option>  </select>');
            break;
        case 2:
            if($("#select-3").val() == "2") {
                $("#pista-text").html("No serà teu el sobre, no? O potser és d'algú que va llegir aquella revista i ha decidit enviar-te una carta com vas demanar. Sigui com sigui, corre! Que el temps s'esgota! Ves a buscar la teva al·lèrgia.")
                $("#pista-input-container").html("");
            } else {
                window.location.replace("blank.html");
            }
            break;
        case 3:
            $("#pista-text").html("Vaja, sembla que hi ha algun record compromès per aquí. Sí, sí, això és teu. Però si tant en saps... Quantes sales té actualment el Grupo Arena?")
            $("#pista-input-container").html('<select class="custom-select" id="select-4">    <option selected="">Escull...</option>     <option value="1">3</option>    <option value="2">4</option>    <option value="3">5</option>    <option value="4">Cap</option>  </select>');
            break;
        case 4:
            if($("#select-4").val() == "3") {
                $("#pista-text").html("Vaja, sembla que encara controles del tema nocturn de Barcelona! I saps què s'hi fa molt? Festa de l'escuma! Llàstima que aquí no en tinguem, però potser en algun lloc on puguis mullar-te hi trobaràs alguna cosa, tic tac, o hauria de dir, xop xop?")
                $("#pista-input-container").html("");
            } else {
                window.location.replace("blank.html");
            }
            break;
        case 5:
            $("#pista-text").html("Ups! Està tancat... I que ni se't passi pel cap saltar la tanca! Per obrir la porta, hauràs d'obrir el candau. I la combinació? Són els mesos que es porten les teves dues germanes.")
            $("#pista-input-container").html("");
            break;
        case 6:
            $("#pista-text").html("Em sembla veure alguna cosa dins... al fons... sota l'unicornio-pegaso.")
            $("#pista-input-container").html("");
            break;
        case 7:
            $("#pista-text").html("Quin és el nombre que hi ha a la carta que has trobat?")
            $("#pista-input-container").html('<select class="custom-select" id="select-5">    <option selected="">Escull...</option>     <option value="1">3</option>    <option value="2">6</option>    <option value="3">9</option>    <option value="4">Canvi de sentit</option>  </select>');
            break;
        case 8:
            if($("#select-5").val() == "1") {
                $("#pista-text").html("Molt bé! Ara ves a la vora del foc a escalfar-te, no volem que agafis fred.")
                $("#pista-input-container").html("");
            } else {
                window.location.replace("blank.html");
            }
            break;
        default:

    }
}
